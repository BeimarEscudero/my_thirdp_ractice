1: 
Averigüe para que sirve la función “addEventListener”, y realice un ejemplo funcional en
un archivo HTML con etiquetas “script”.
La función addEventListener de JavaScript se usa para asignar un evento a un elemento HTML y proporcionar la función que se ejecutará cuando ocurra ese evento. permite que el código JavaScript reaccione a las interacciones del usuario, como hacer clic en un botón, mover el mouse sobre un elemento o enviar un formulario.
ejemplo:

<!DOCTYPE html>
<html>
<head>
  <title>Ejemplo de addEventListener</title>
</head>
<body>
  <button id="myButton">Haz clic</button>

  <script>
    const button = document.getElementById('myButton');
    function handleClick() {
      console.log('¡Se hizo clic en el botón!');
    }
    button.addEventListener('click', handleClick);
  </script>
</body>
</html>
2:
Averigüe y realice un ejemplo funcional de la creación, recorrido de posiciones y obtener la
longitud de un vector en JS.
En JavaScript, puedes utilizar un array para crear un vector y luego recorrerlo para obtener las posiciones y la longitud.
<!-- const vector = [2, 4, 6, 8, 10];
console.log("Recorrido de posiciones:");
for (let i = 0; i < vector.length; i++) {
  console.log("Posición", i, ":", vector[i]);
}
const longitud = vector.length;
console.log("Longitud del vector:", longitud); -->

En este ejemplo, creamos un vector llamado vector con los números [2, 4, 6, 8, 10]. Luego, utilizamos un bucle for para recorrer las posiciones del vector e imprimimos cada valor en la consola. Por último, utilizamos la propiedad length del vector para obtener su longitud y lo mostramos en la consola.
3:
Averigüe y realice ejemplos funcionales de los tipos de funciones que hay en JS.

*Funciones declaradas:
<!-- function suma(a, b) {
  return a + b;
}

console.log(suma(2, 3)); // Resultado: 5
 -->
*Funciones expresadas:
<!-- const resta = function(a, b) {
  return a - b;
};

console.log(resta(5, 3)); // Resultado: 2 -->
*Funciones recursivas:  
<!-- function factorial(n) {
  if (n === 0) {
    return 1;
  }
  
  return n * factorial(n - 1);
}

console.log(factorial(5)); // Resultado: 120 -->
5:
Cual es la diferencia entre JavaScript y TypeScript; y crea una variable, que almacene un
número y luego hacer que se muestre en la consola en los dos lenguajes. Para ello, crear un
archivo ts y js.
JavaScript y TypeScript son dos lenguajes de programación relacionados, pero con algunas diferencias clave:

JavaScript:

<!-- Es un lenguaje de programación interpretado de alto nivel.
Es un lenguaje de programación de scripting utilizado principalmente para desarrollar aplicaciones web interactivas.
Es flexible y dinámico, lo que significa que no se requiere una compilación explícita antes de ejecutar el código.
No tiene un sistema de tipos estricto y permite cambios dinámicos en las variables. -->

TypeScript:

<!-- Es un superconjunto de JavaScript, lo que significa que cualquier código JavaScript válido también es código TypeScript.
Es un lenguaje de programación de tipado estático.
Agrega características adicionales a JavaScript, como el sistema de tipos estático, interfaces, clases y módulos.
Requiere una etapa de compilación en la que el código TypeScript se convierte en JavaScript antes de ser ejecutado por el navegador o el entorno de ejecución. -->

